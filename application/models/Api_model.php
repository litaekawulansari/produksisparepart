<?php
class Api_model extends CI_Model
{
  function fetch_all()
  {
    $this->db->order_by('id', 'ASC');
    return $this->db->get('sparepart');
  }

  //menambahkan sparepart
  function insert_api($data)
  {
    $this->db->insert('sparepart', $data);
    if($this->db->affected_rows() > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }


  // mengambil data untuk diupdate
  function fetch_single_user($user_id)
  {
    $this->db->where("id", $user_id);
    $query = $this->db->get('sparepart');
    return $query->result_array();
  }


  //untuk mengubah data
  function update_api($user_id, $data)
  {
    $this->db->where("id", $user_id);
    $this->db->update("sparepart", $data);
  }


  //mengubah status sparepart
  function delete_single_user($user_id,$data)
  {
    $this->db->where("id", $user_id);
    $this->db->update("sparepart", $data);
  }
}
?>
