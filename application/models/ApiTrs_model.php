<?php
class ApiTrs_model extends CI_Model
{
  private $_table = "sparepart";
  private $_tableL = "laporan";

  function fetch_all()
  {
    $this->db->order_by('id', 'ASC');
    return $this->db->get('laporan');
  }


  //mendapatkan sparepart untuk dropdown list
  function getAllSparepart()
  {
    $where = array(
      'status' => "1"
    );

    $this->db->where($where);
    return $this->db->get($this->_table)->result();
  }

  //menambahkan dan mengubah sparepart
  function insert_api($data,$qty,$id)
  {

    //menambahkan sparepart
    $this->db->insert('laporan', $data);

    //mengurangi stok sparepart
    $this->db->set('kuantitas', 'kuantitas-'.$qty, FALSE);
    $this->db->where("id", $id);
    $this->db->update('sparepart');

    

    if($this->db->affected_rows() > 0)
    {
      return true;
    }
    else
    {
      return false;
    }
  }
}
?>
