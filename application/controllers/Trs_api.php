<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trs_api extends CI_Controller {

  public function __construct(){
    parent::__construct();
    $this->load->model('ApiTrs_model');
  }

  function index()
  {
    $data["user1"] = $this->ApiTrs_model->getAllSparepart();
    
    $this->load->view('template/top');
    $this->load->view('apitrs_view',$data);
    $this->load->view('template/bottom');
  }

  function action()
  {
    if($this->input->post('data_action'))
    {
     $data_action = $this->input->post('data_action');


    if($data_action == "Insert")
    {
      $api_url = "http://localhost/Coba/apitrs/insert";


      $form_data = array(
       'id_sp'  => $this->input->post('id_sp'),
       'tanggal'   => date('Y-m-d'),
       'keterangan' => $this->input->post('keterangan'),
       'tempat' => $this->input->post('tempat'),
       'kuantitas' => $this->input->post('kuantitas')
     );

      $client = curl_init($api_url);

      curl_setopt($client, CURLOPT_POST, true);

      curl_setopt($client, CURLOPT_POSTFIELDS, $form_data);

      curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($client);

      curl_close($client);

      echo $response;
    }


    if($data_action == "fetch_all")
    {
      $api_url = "http://localhost/Coba/apitrs";

      $client = curl_init($api_url);

      curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

      $response = curl_exec($client);

      curl_close($client);

      $result = json_decode($response);

      $output = '';

      if (is_countable($result) && count($result) > 0)
      {
       foreach($result as $row)
       {
        $output .= '
        <tr>
        <td>'.$row->id.'</td>
        <td>'.$row->id_sp.'</td>
        <td>'.$row->tanggal.'</td>
        <td>'.$row->keterangan.'</td>
        <td>'.$row->tempat.'</td>
        <td>'.$row->kuantitas.'</td>
        </tr>'
        ;
      }
    }
    else
    {
     $output .= '
     <tr>
     <td colspan="4" align="center">No Data Found</td>
     </tr>
     ';
   }

   echo $output;
 }
}
}

}

?>
