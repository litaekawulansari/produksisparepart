<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ApiTrs extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('apitrs_model');
  $this->load->library('form_validation');
}

function index()
{
  $data = $this->apitrs_model->fetch_all();
  echo json_encode($data->result_array());
}


//menambahkan data produksi dari sparepart
function insert()
{
  $this->form_validation->set_rules("keterangan", "Keterangan", "required");
  $this->form_validation->set_rules("tempat", "Tempat", "required");
  $this->form_validation->set_rules("kuantitas", "Kuantitas", "required");
  $array = array();
  if($this->form_validation->run())
  {
    $qty = $this->input->post('kuantitas');
    $id = $this->input->post('id_sp');
   $data = array(
     'id_sp'  => $this->input->post('id_sp'),
     'tanggal'   => date('Y-m-d'),
     'keterangan' => $this->input->post('keterangan'),
     'tempat' => $this->input->post('tempat'),
     'kuantitas' => $this->input->post('kuantitas')
   );

   //insert produksi dan update stok sparepart
   $this->apitrs_model->insert_api($data,$qty,$id);

   $array = array(
    'success'  => true
  );
 }
 else
 {
   $array = array(
    'error'    => true,
    'keterangan_error' => form_error('keterangan'),
    'tempat_error' => form_error('tempat'),
    'kuantitas_error' => form_error('kuantitas')
  );
 }
 echo json_encode($array, true);
}


}