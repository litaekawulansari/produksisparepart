<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

 public function __construct()
 {
  parent::__construct();
  $this->load->model('api_model');
  $this->load->library('form_validation');
}

function index()
{
  $data = $this->api_model->fetch_all();
  echo json_encode($data->result_array());
}


function insert()
{
  //validasi form
  $this->form_validation->set_rules("nama", "Nama", "required");
  $this->form_validation->set_rules("kuantitas", "Kuantitas", "required");
  $array = array();


  if($this->form_validation->run())
  {
    //data inputan 
   $data = array(
    'nama' => trim($this->input->post('nama')),
    'kuantitas'  => trim($this->input->post('kuantitas')),
    'status'  => 1
  );
   $this->api_model->insert_api($data);
   $array = array(
    'success'  => true
  );
 }
 else
 {
   $array = array(
    'error'    => true,
    'nama_error' => form_error('nama'),
    'kuantitas_error' => form_error('kuantitas')
  );
 }
 echo json_encode($array, true);
}

//untuk mengambil data view update
function fetch_single()
{
  if($this->input->post('id'))
  {
   $data = $this->api_model->fetch_single_user($this->input->post('id'));
   foreach($data as $row)
   {
    $output['nama'] = $row["nama"];
    $output['kuantitas'] = $row["kuantitas"];
  }
  echo json_encode($output);
}
}

//untuk mengubah sparepart
function update()
{
  $this->form_validation->set_rules("nama", "Nama", "required");
  $this->form_validation->set_rules("kuantitas", "Kuantitas", "required");
  $array = array();
  if($this->form_validation->run())
  {
   $data = array(
    'nama' => trim($this->input->post('nama')),
    'kuantitas'  => trim($this->input->post('kuantitas'))
  );
   $this->api_model->update_api($this->input->post('id'), $data);
   $array = array(
    'success'  => true
  );
 }
 else
 {
   $array = array(
    'error'    => true,
    'nama_error' => form_error('nama'),
    'kuantitas_error' => form_error('kuantitas')
  );
 }
 echo json_encode($array, true);
}


//menon aktifkan sparepart
function delete()
{
  if($this->input->post('id'))
  {
    $data = array('status' => 0);
    if($this->api_model->delete_single_user($this->input->post('id'),$data))
    {
      $array = array(
       'success' => true
     );
    }
    else
    {
      $array = array(
       'error' => true
     );
    }
    echo json_encode($array,true);
  }
}


//mengaktifkan sparepart
function deleteAktif()
{
  if($this->input->post('id'))
  {
    $data = array('status' => 1);
    if($this->api_model->delete_single_user($this->input->post('id'),$data))
    {
      $array = array(
       'success' => true
     );
    }
    else
    {
      $array = array(
       'error' => true
     );
    }
    echo json_encode($array,true);
  }
}

}