
<!-- view -->
    <div class="container">
        <br />
        <h3 align="center">CRUD Master Sparepart</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Sparepart</h3>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" id="add_button" class="btn btn-info btn-xs">Add Sparepart</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <span id="success_message"></span>
                <table class="table table-bordered table-striped">
                    <thead align="center">
                        <tr>
                            <th align="center">ID</th>
                            <th align="center">Nama</th>
                            <th align="center">Kuantitas</th>
                            <th align="center">Status</th>
                            <th colspan="2" align="center">Action</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>


<!-- modal untuk update dan insert -->
<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Sparepart</h4>
                </div>
                <div class="modal-body">
                    <label>Enter Nama</label>
                    <input type="text" name="nama" id="nama" class="form-control" />
                    <span id="nama_error" class="text-danger"></span>
                    <br />
                    <label>Enter Kuantitas</label>
                    <input type="number" name="kuantitas" id="kuantitas" class="form-control" />
                    <span id="kuantitas_error" class="text-danger"></span>
                    <br />
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id" />
                    <input type="hidden" name="data_action" id="data_action" value="Insert" />
                    <input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" language="javascript" >

    //master sparepart
    $(document).ready(function(){

        function fetch_data()
        {
            $.ajax({
                url:"<?php echo base_url(); ?>test_api/action",
                method:"POST",
                data:{data_action:'fetch_all'},
                success:function(data)
                {
                    $('tbody').html(data);
                }
            });
        }

        fetch_data();

        $('#add_button').click(function(){
            $('#user_form')[0].reset();
            $('.modal-title').text("Add Sparepart");
            $('#action').val('Add');
            $('#data_action').val("Insert");
            $('#userModal').modal('show');
        });

        $(document).on('submit', '#user_form', function(event){
            event.preventDefault();
            $.ajax({
                url:"<?php echo base_url() . 'test_api/action' ?>",
                method:"POST",
                data:$(this).serialize(),
                dataType:"json",
                success:function(data)
                {
                    if(data.success)
                    {
                        $('#user_form')[0].reset();
                        $('#userModal').modal('hide');
                        fetch_data();
                        if($('#data_action').val() == "Insert")
                        {
                            $('#success_message').html('<div class="alert alert-success">Data Inserted</div>');
                        }
                    }

                    if(data.error)
                    {
                        $('#nama_error').html(data.nama_error);
                        $('#kuantitas_error').html(data.kuantitas_error);
                    }
                }
            })
        });

        $(document).on('click', '.edit', function(){
            var user_id = $(this).attr('id');
            $.ajax({
                url:"<?php echo base_url(); ?>test_api/action",
                method:"POST",
                data:{user_id:user_id, data_action:'fetch_single'},
                dataType:"json",
                success:function(data)
                {
                    $('#userModal').modal('show');
                    $('#nama').val(data.nama);
                    $('#kuantitas').val(data.kuantitas);
                    $('.modal-title').text('Edit Sparepart');
                    $('#user_id').val(user_id);
                    $('#action').val('Edit');
                    $('#data_action').val('Edit');
                }
            })
        });

        $(document).on('click', '.delete', function(){
            var user_id = $(this).attr('id');
            if(confirm("Are you sure you want to delete this?"))
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>test_api/action",
                    method:"POST",
                    data:{user_id:user_id, data_action:'Delete'},
                    dataType:"JSON",
                    success:function(data)
                    {
                        $('#success_message').html('<div class="alert alert-success">Data Non Aktif</div>');
                        fetch_data();
                    }
                })
            }
        });

        $(document).on('click', '.deleteAktif', function(){
            var user_id = $(this).attr('id');
            if(confirm("Are you sure you want to delete this?"))
            {
                $.ajax({
                    url:"<?php echo base_url(); ?>test_api/action",
                    method:"POST",
                    data:{user_id:user_id, data_action:'DeleteAktif'},
                    dataType:"JSON",
                    success:function(data)
                    {
                        $('#success_message').html('<div class="alert alert-success">Data Aktif</div>');
                        fetch_data();
                    }
                })
            }
        });
        
    });
</script>