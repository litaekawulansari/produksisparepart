<html>
<head>
    <title>Produksi Sparepart</title>
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    
</head>
<body>
    <div class="container">
        <br />
        <h3 align="center">Create Produksi Sparepart</h3>
        <br />
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="panel-title">Laporan Produksi Sparepart</h3>
                    </div>
                    <div class="col-md-6" align="right">
                        <button type="button" id="add_button" class="btn btn-info btn-xs">Add Produksi</button>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <span id="success_message"></span>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID Produksi</th>
                            <th>ID Sparepart</th>
                            <th>Tanggal</th>
                            <th>Keterangan</th>
                            <th>Tempat</th>
                            <th>Kuantitas</th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</body>
</html>

<div id="userModal" class="modal fade">
    <div class="modal-dialog">
        <form method="post" id="user_form">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Produksi</h4>
                </div>
                <div class="modal-body">
                    <div class="form-check">
                        <label for="prodi">Sparepart</label>
                        <select class="form-control" ID="id_sp" name="id_sp" required>
                            <?php foreach ($user1 as $u) {?>
                                <option value="<?=$u->id?>"> <?php echo $u->nama?></option>';
                            <?php }?>
                        </select>
                    </div>
                    <br />
                    <label>Enter keterangan</label>
                    <input type="text" name="keterangan" id="keterangan" class="form-control" />
                    <span id="keterangan_error" class="text-danger"></span>
                    <br />
                    <div class="form-check">
                        <label for="prodi">Line Produksi</label>
                        <select class="form-control" ID="tempat" name="tempat" required>
                            <option value="Line 1">Line 1</option>';
                            <option value="Line 2">Line 2</option>';
                            <option value="Line 3">Line 3</option>';
                            <option value="Line 4">Line 4</option>';
                            <option value="Line 5">Line 5</option>';
                            <option value="Line 6">Line 6</option>';
                            <option value="Line 7">Line 7</option>';
                        </select>
                    </div>
                    <br />
                    <label>Enter kuantitas produksi</label>
                    <input type="number" name="kuantitas" id="kuantitas" class="form-control" />
                    <span id="kuantitas_error" class="text-danger"></span>
                    <br />
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="user_id" id="user_id" />
                    <input type="hidden" name="data_action" id="data_action" value="Insert" />
                    <input type="submit" name="action" id="action" class="btn btn-success" value="Add" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </form>
    </div>
</div>

<script type="text/javascript" language="javascript" >
    $(document).ready(function(){

        function fetch_data()
        {
            $.ajax({
                url:"<?php echo base_url(); ?>trs_api/action",
                method:"POST",
                data:{data_action:'fetch_all'},
                success:function(data)
                {
                    $('tbody').html(data);
                }
            });
        }

        fetch_data();

        $('#add_button').click(function(){
            $('#user_form')[0].reset();
            $('.modal-title').text("Add Produksi");
            $('#action').val('Add');
            $('#data_action').val("Insert");
            $('#userModal').modal('show');
        });

        $(document).on('submit', '#user_form', function(event){
            event.preventDefault();
            $.ajax({
                url:"<?php echo base_url() . 'trs_api/action' ?>",
                method:"POST",
                data:$(this).serialize(),
                dataType:"json",
                success:function(data)
                {
                    if(data.success)
                    {
                        $('#user_form')[0].reset();
                        $('#userModal').modal('hide');
                        fetch_data();
                        if($('#data_action').val() == "Insert")
                        {
                            $('#success_message').html('<div class="alert alert-success">Data Inserted, Stok Sparepart Updated</div>');
                        }
                    }

                    if(data.error)
                    {
                        $('#nama_error').html(data.nama_error);
                        $('#kuantitas_error').html(data.kuantitas_error);
                    }
                }
            })
        });
        
    });
</script>