                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>Copyright &copy; Your Website 2019</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>
    <!-- Core plugin JavaScript-->
    <script src="<?=base_url().'/assets/'?>vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?=base_url().'/assets/'?>vendor/js/sb-admin-2.min.js"></script>

    <!-- DataTable -->
    <script src="<?=base_url()?>assets/vendor/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets/vendor/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- TimePicker -->
    <script src="<?=base_url()?>assets/vendor/timepicker/jquery.timepicker.min.js"></script>
    <!-- jqConfrim -->
    <script src="<?=base_url()?>assets/js/jquery-confirm.min.js"></script>
    <!-- Chart -->
    <script src="<?=base_url()?>assets/js/Chart.min.js"></script>
    <!-- Rater -->
    <script src="<?=base_url()?>assets/js/rater.min.js"></script>
    <!-- Print -->
    <script src="<?=base_url()?>assets/js/printThis.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <!-- Bootstrap -->
    
    <script>
    function notif(title, msg) {
        $.alert({
            title: title,
            content: msg
        });
    }
    </script>

    <? if($script != ''):?>
        <script src="<?=base_url()?>assets/js/<?=$script?>"></script>
    <?endif;?>

    <?php if(isset($misc)){?>
        <?php echo $misc?>
    <?php } ?>



</body>

</html>

