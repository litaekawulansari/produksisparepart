<div class="row">
	<div class="col-8">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Wellcome!</h6>
			</div>
			<div class="card-body">
				<p style="height:100%">
					Wellcome to ez jam plus!<br>
					This is the place if both teacher and student go get what they both need!
					<br>
					<br>
					Hey Teacher! Need some man power? we have you covered! just post a new Quest and get your troops ASAP!
					And what about you students? Need some jam plus? Just apply for an open quest!
					<br>
					Cant wait long enough? or need jam plus in urgent? Just submit some jam plus submission to your teacher!
					<br>
					And just wait till they accept it!
					<br>
					<br>
					Sincerely, Dev
				</p>
			</div>
		</div>
	</div>
	<div class="col-4">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary"><?=$donutTitle?></h6>
			</div>
			<div class="card-body">
				<div class="row">
					<canvas id="canva3" style="height:100%"></canvas>
				</div>
			</div>
		</div>
	</div>
</div>
<?php if($user['role']!=2){?>
<div class="row" id='gra'>
	<div class="col-6">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Number of Quest Made per Month</h6>
			</div>
			<div class="card-body">
				<div class="row" style="padding-left:1rem;padding-right:1rem;">
					<select id='yearddl' class="btn btn-secondary dropdown-toggle">
						<?php for($i=intval(date("Y"));$i >= 2019 ;$i--){?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					</select>
				</div>
				<div class="row" style="padding-left:1rem;padding-right:1rem;" style="height:50%">
					<canvas id="canva1" height="100%"></canvas>
				</div>
			</div>
		</div>
	</div>
	<div class="col-6">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Number of Submission Submitted per Month</h6>
			</div>
			<div class="card-body">
				<div class="row" style="padding-left:1rem;padding-right:1rem;">
					<select id='yearddl2' class="btn btn-secondary dropdown-toggle">
						<?php for($i=intval(date("Y"));$i >= 2019 ;$i--){?>
						<option value="<?=$i?>"><?=$i?></option>
						<?php }?>
					</select>
				</div>
				<div class="row" style="padding-left:1rem;padding-right:1rem;" style="height:50%">
					<canvas id="canva2" height="100%" ></canvas>
				</div>
			</div>
		</div>
	</div>
</div>
<?php }else{?>
<div class="row" id='gra'>
	<div class="col">
		<div class="card shadow mb-4">
			<div class="card-header py-3">
				<h6 class="m-0 font-weight-bold text-primary">Your Rating</h6>
			</div>
			<div class="card-body">
				<center>
				<h1><i class="fas fa-star"></i>: <?=$rating?> / 5</h1></center>
			</div>
		</div>
	</div>
</div>
<?php }?>