<!DOCTYPE html>
<html lang="en">
<body class="bg-gradient-light">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-5 col-lg-6 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-12">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">Your Ez Jam Plus Login Credential</h1>
                  </div>
                  <form  method="POST">
                            <label>Username</label>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" 
                      id="id" name="id" aria-describedby="emailHelp" value="<?=$id?>">
                    </div>
                            <label>Password</label>
                    <div class="form-group">
                      <input type="text" class="form-control form-control-user" 
                      id="pass" name="pass" value="<?=$pass?>">
                    </div>
                    <br/>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->

</body>

</html>
