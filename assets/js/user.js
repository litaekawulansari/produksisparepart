var table;
table = $('#user').DataTable({
	serverSide: true,
	order: [0, "asc"],
	processing: true,
	ajax: {
		url: window.location.href + '/fetch_data',
		type: "POST"
	},
	"columnDefs": [
		{
			"targets": [5],
			"orderable": false
		}
	],
	"drawCallback": function( settings ) {
        $('.hasConf').confirm({
			backgroundDismiss: true,
			title: 'Confirmation',
			content: 'Are you sure?'
		});
    }
});
$(document).ready(function () {
});