var table;
	table = $('#prodi').DataTable({
		serverSide: true,
		order: [0, "asc"],
		processing: true,
		ajax: {
			url: window.location.href+'/fetch_data',
			type: "POST"
		},
		"columnDefs": [
			{
				"targets": [3,4],
				"orderable": false
			}
		],
		"drawCallback": function( settings ) {
			$('.hasConf').confirm({
				backgroundDismiss: true,
				title: 'Confirmation',
				content: 'Are you sure?'
			});
		}
	});
	