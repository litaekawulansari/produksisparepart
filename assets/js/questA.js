$(document).ready(function(){
	//initiate timepicker
	$('input.timepicker').timepicker({
	timeFormat: 'HH:mm',
	interval: 60,
	minTime: '8',
	maxTime: '18:00',
	defaultTime: '8',
	dynamic: false,
	dropdown: true,
	scrollbar: true,
	change: function(time) {
		var element = $(this);
		console.log(element[0].value);
		}
	});

	//set job start date (submissionDeadline)
	document.getElementById("submissionDeadline").valueAsDate = new Date();
});

$(function () {
	$('#title, #name_long').keydown(function (e) {
		if (e.altKey) {
			e.preventDefault();
		} else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
				e.preventDefault();
			}
		}
	});
});