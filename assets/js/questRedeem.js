function setDateTime(date, time){
	$('#submissionDeadline').val(date);
	
	$('input.timepicker').timepicker({
	timeFormat: 'HH:mm',
	interval: 60,
	minTime: '8',
	maxTime: '18:00',
	defaultTime: time,
	dynamic: false,
	dropdown: false,
	scrollbar: true,
	change: function(time) {
		var element = $(this);
		console.log(element[0].value);
		}
	});
}


function print() {
	$('#form').printThis({
		canvas: true          // copy canvas content
	});
}