var table;
table = $('#finishTable').DataTable({
	serverSide: true,
	order: [],
	processing: true,
	ajax: {
		url: window.location.href+'/fetch_data',
		type: "POST"
	},
	"columnDefs": [
		{
			"targets": [2,3],
			"orderable": false
		}
	], 
	"language": {
		"emptyTable": "No quest available, check again later!"
	},
	"bInfo" : false
});

function toggleGiven(questId, participantId){
	var data = $.ajax({
		url : window.location.href+'/toggleGiven',
		type : 'POST',
		data : {questId:questId, participantId:participantId},
		dataType : 'JSON',
		complete(f){
			if(f.responseText){
				console.log(f.responseText);
				table.ajax.reload();
			}
			else{
				alert('Oh no! something went wrong!');
			}
		}
	})
};