var table;
table = $('#rateTabel	').DataTable({
	serverSide: true,
	order: [],
	processing: true,
	ajax: {
		url: window.location.href+'/fetch_data',
		type: "POST"
	},
	"columnDefs": [
		{
			"targets": [4],
			"orderable": false
		}
	], 
	"language": {
		"emptyTable": "No quest available, check again later!"
	},
	"bInfo" : false,
	"drawCallback": function( settings ) {
        $('.rate').rate({
            max_value: 5,
            step_size: 0.5,
            initial_value: 5,
			change_once: true
		});

		$(".rate").on("afterChange", function (ev, data) {
            participantId = String(this.dataset.participantId);
            rating = data.to;
            $.ajax({
               type: "POST",
               url: document.location.href+"/rate",
               dataType: 'json',
               data: {
				   participantId: participantId,
				   rating: data.to,
				   rateCount: this.dataset.rateCount
				},
               error: function (XMLHttpRequest, textStatus, errorThrown) {
               },
               success: function (jqXHR, status) {
               }
            });
        });
    }
});

var a = 1;