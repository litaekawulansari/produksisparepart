questPerMonth = $('#canva1');
submissionPerMonth = $('#canva2');
activeProdi = $('#canva3');
initData = [];

$(function () {

	barData1 = [];
	var barChart1;
	//load quest per month
	$.ajax({
		url: document.location.href + '/questPerMonth',
		type: 'post',
		data: { year: new Date().getFullYear() },
		dataType: 'JSON',
		success: function (a) {
			var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var i = 0; i < a.length; i++) {
				data[i] = parseInt(a[i].count);
			}
			barData1 = data;
		},
		complete: function (e) {
			barChart1 = new Chart(questPerMonth, {
				type: 'line',
				data: {
					labels: [
						'January', 'February', 'March', 'May', 'April',
						'June', 'July', 'Aprii', 'Agustus', 'September',
						'November', 'December'
					],
					datasets: [{
						label: 'Quest Per Month',
						data: barData1,
						fill: false,
						pointRadius: 5,
						pointHoverRadius: 10,
						lineTension: 0.2
					}]
				}
			});
		}
	});

	barData2 = [];
	var barChart2;
	//load quest per month
	$.ajax({
		url: document.location.href + '/submissionPerMonth',
		type: 'post',
		data: { year: new Date().getFullYear() },
		dataType: 'JSON',
		success: function (a) {
			var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
			for (var i = 0; i < a.length; i++) {
				data[i] = parseInt(a[i].count);
			}
			barData2 = data;
		},
		complete: function (e) {
			barChart2 = new Chart(submissionPerMonth, {
				type: 'line',
				data: {
					labels: [
						'January', 'February', 'March', 'May', 'April',
						'June', 'July', 'Aprii', 'Agustus', 'September',
						'November', 'December'
					],
					datasets: [{
						label: 'Quest Per Month',
						data: barData2,
						fill: false,
						pointRadius: 5,
						pointHoverRadius: 10,
						lineTension: 0.2
					}]
				}
			});
		}
	});

	donutData = [];
	donutLabel = [];
	var donutChart;
	//test
	$.ajax({
		url: document.location.href + '/topProdi',
		type: 'post',
		dataType: 'JSON',
		success: function (a) {
			var label = [];
			var data = [];
			for (var i = 0; i < a.length; i++) {
				donutData[i] = parseInt(a[i].count);
				donutLabel[i] = a[i].prodi;
			}
			if (role==2){
				donutLabel = a.prodi;
				donutData = a.count;
			}
		},
		complete: function (e) {
			donutChart = new Chart(activeProdi, {
				type: 'doughnut',
				data: {
					datasets: [{
						data: donutData,
						backgroundColor: [
							'mediumpurple',
							'lightpink',
							'lightgreen',
							'lightsalmon',
							'aqua',
							'mediumorchid',
							'peachpuff'
						]
					}],
					labels: donutLabel
				}
			});
		}
	});

	

	$('#yearddl').change(function () {
		$.ajax({
			url: document.location.href + '/questPerMonth',
			type: 'post',
			data: { year: this.value },
			dataType: 'JSON',
			success: function (a) {
				var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
				for (var i = 0; i < a.length; i++) {
					data[i] = parseInt(a[i].count);
				}

				barChart1.data.datasets[0].data = data;
				barChart1.update();
			}
		});
	});

	$('#yearddl2').change(function () {
		$.ajax({
			url: document.location.href + '/submissionPerMonth',
			type: 'post',
			data: { year: this.value },
			dataType: 'JSON',
			success: function (a) {
				var data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
				for (var i = 0; i < a.length; i++) {
					data[i] = parseInt(a[i].count);
				}

				barChart2.data.datasets[0].data = data;
				barChart2.update();
			}
		});
	});
});
