function addParticipant() {
	var table = document.getElementById("participant");
	var txtId = document.getElementById('participantID');

	var id = { 
		'id'     : txtId.value
	};

	data = $.ajax({
		url : 'getUser',
		type : 'POST',
		data : id,
		dataType : 'JSON',
		complete(f){
			if(f.responseJSON==null)
				alert('Invalid NIM');
			else{
				var row = table.insertRow(1);
				var cell1 = row.insertCell(0);
				var cell2 = row.insertCell(1);
				var cell3 = row.insertCell(2);
				var cell4 = row.insertCell(3);
				cell1.innerHTML = "<input type=\"hidden\" name=\"participants[]\" value="+f.responseJSON.uId+">"+f.responseJSON.uId;
				cell2.innerHTML = f.responseJSON.name;
				cell3.innerHTML = f.responseJSON.prodiName;
				cell4.innerHTML = "<input type=\"button\" value=\"Remove\" onclick=\"deleteRow(this)\" class='btn btn-primary'/>";
			}
		}
	});
	txtId.value=null;
}

function deleteRow(btn) {
	var row = btn.parentNode.parentNode;
	row.parentNode.removeChild(row);
}

$('input.timepicker').timepicker({
	timeFormat: 'HH:mm',
	interval: 60,
	minTime: '8',
	maxTime: '18:00',
	defaultTime: '8',
	dynamic: false,
	dropdown: true,
	scrollbar: true,
	change: function(time) {
		var element = $(this);
		console.log(element[0].value);
	}
});


$(function () {
	$('#title').keydown(function (e) {
		if (e.altKey) {
			e.preventDefault();
		} else {
			var key = e.keyCode;
			if (!((key == 8) || (key == 32) || (key == 46) || (key >= 35 && key <= 40) || (key >= 65 && key <= 90))) {
				e.preventDefault();
			}
		}
	});
});
