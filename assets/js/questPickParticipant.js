
var table;
$( document ).ready( initiateDT );

function initiateDT(){
	table = $('#participantsTable').DataTable({
		serverSide: true,
		order: [],
		processing: true,
		ajax: {
			url: window.location.href+'/fetch_data',
			type: "POST"
		},
		"columnDefs": [
			{
				"targets": [3,4],
				"orderable": false
			}
		], 
		"language": {
			"emptyTable": "No quest available, check again later!"
		}
	});
}

function pickParticipant(questId, participantId){
	var data = $.ajax({
		url : window.location.href+'/pickParticipant',
		type : 'POST',
		data : {questId:questId, participantId:participantId},
		dataType : 'JSON',
		complete(f){
			if(f.responseText){
				var text = 'Participant Slot: ';
				var count = JSON.parse(f.responseText).count;
				var limit = JSON.parse(f.responseText).limit;
				$('#participantSlot').html(text+count+'/'+limit);
				table.ajax.reload();
			}
			else{
				alert('Oh no! something went wrong!');
			}
		}
	})
};