var table;
table = $('#quest').DataTable({
	serverSide: true,
	order: [1,'desc'],
	processing: true,
	ajax: {
		url: window.location.href+'/fetch_data',
		type: "POST"
	},
	"columnDefs": [
		{
			"targets": [6,8],
			"orderable": false
		}
	], 
	"language": {
		"emptyTable": "No quest available, check again later!"
	},
	"columns": [
		{
			"class":          "details-control",
			"orderable":      false,
			"data":           'detail',
			"defaultContent": ""
		},
		{ "data": 0 },
		{ "data": 1 },
		{ "data": 2 },
		{ "data": 3 },
		{ "data": 4 },
		{ "data": 5 },
		{ "data": 6 },
		{ "data": 7 },
	],
	"bInfo" : false,
	"drawCallback": function( settings ) {
        $('.hasConf').confirm({
			backgroundDismiss: true,
			title: 'Confirmation',
			content: 'Are you sure?'
		});
    }
});

var shownRow = null;
$('#quest tbody').on( 'click', 'tr td.details-control', function () {

	if(shownRow != null && shownRow != this)shownRow.click();
	shownRow = this;

	var tr = $(this).closest('tr');
	var row = table.row( tr );
	var idx = $.inArray( tr.attr('id') );

	var rotVal = getRotationDegrees($(this).find('img'));
	if ( row.child.isShown() ) {
		if(shownRow == this) shownRow = null;

		rotVal = 0;
		$('div.slider', row.child()).slideUp(function(){
			row.child.hide();
		});

		$(this).find('img').css({
			"transform": "rotate("+rotVal+"deg)", 
			"transition": "500ms ease-out",
			"box-shadow": "0 0 1px rgba(255,255,255,0)"
		});
	} else {
		rotVal = 45;
		row.child( format( row.data() ), 'no-padding' ).show();
		tr.addClass('shown');
		$('div.slider', row.child()).slideDown();

		//expand button animation
		$(this).find('img').css({
			"transform": "rotate("+rotVal+"deg)", 
			"transition": "500ms ease-out",
			"box-shadow": "0 0 1px rgba(255,255,255,0)"
		});
	}

} );

function format ( d ) {
	return '<div class="slider" style="padding:0rem;">'+
				'<div class="card border-left-primary" style="padding:1rem;">'+
					'<b>Jam Plus Description: </b>'+
					d['description']+'<br/>'+'<br>' +
					'<hr/>' + 
					'<b>From</b>' + 
					d['fromUserName'] + 
					'<div class="form-row">' +
						'<div class="col-6">' +
							'<b>Jam Plus</b>' + '<br>' +
							d['jamPlusAmount'] + 
						'</div>' +
						'<div class="col-6">' +
							'<b>Participant</b>' + '<br>' +
							d['participantAmount'] + 
						'</div>' +
					'</div>' +
					'<div class="form-row">' +
						'<div class="col-6">' +
							'<b>Job Date and Time</b>' + '<br>' +
							d['submissionDeadline']+ ' At ' + d['submissionDeadlineTime'] + '<br>' +
						'</div>' +
						'<div class="col-6">' +
							'<b>Job Type</b>' + '<br>' +
							d['jobType'] + '<br>' +
						'</div>' +
					'</div>' +
				'</div>' +
			'</div>';
	}

function getRotationDegrees(obj) {
    var matrix = obj.css("-webkit-transform") ||
    obj.css("-moz-transform")    ||
    obj.css("-ms-transform")     ||
    obj.css("-o-transform")      ||
    obj.css("transform");
    if(matrix !== 'none') {
        var values = matrix.split('(')[1].split(')')[0].split(',');
        var a = values[0];
        var b = values[1];
        var angle = Math.round(Math.atan2(b, a) * (180/Math.PI));
    } else { var angle = 0; }
    return (angle < 0) ? angle + 360 : angle;
}

$(document).ready(function(){
	function someFunction(){
		alert('You just finish a quest! Nice!');
	}
});
