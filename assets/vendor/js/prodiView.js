
var table;
$(document).ready(function () {
    //datatables
    table = $('#prodi').DataTable({
        "processing": true,
        "serverSide": true,
        "order": [],

        "ajax": {
            "url": "<?php echo base_url('prodi/getData')?>",
            "type": "POST"
        },


        "columnDefs": [
            {
                "targets": [0],
                "orderable": false,
            },
        ],
    });
});